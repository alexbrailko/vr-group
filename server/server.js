const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const cors = require('cors');
const fs = require('fs'); 
const path = require('path');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});
const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(cors());

app.get('/files', (req, res) => {
  const filesDirectory = path.join(__dirname, 'files');

  // Read the contents of the 'files' directory
  fs.readdir(filesDirectory, (err, files) => {
    if (err) {
      res.status(500).json({ error: 'Failed to read the directory.' });
    } else {
      const fileList = fetchFileList();
      res.json(fileList);
    }
  });
}); 

app.get('/files/:filename', (req, res) => {
  const { filename } = req.params;
  const filePath = __dirname + '/files/' + filename;

  if (fs.existsSync(filePath)) {
    // Read the JSON file and send it as a response
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
        res.status(500).json({ error: 'Failed to read the file.' });
      } else {
        res.json(JSON.parse(data));
      }
    });
  } else {
    res.status(404).json({ error: 'File not found' });
  }
});

io.on('connection', (socket) => {
  console.log('A user connected');

  // Handle updating a file's content
  socket.on('update', ({ fileName, content }) => {
    const filePath = path.join(__dirname, 'files', fileName);
 
    if (fs.existsSync(filePath)) {
      // Write the edited content to the file
      fs.writeFileSync(filePath, JSON.stringify(content, null, 2));

      const fileList = fetchFileList();
      // send to all connected clients
      io.emit('updated', {fileList, newFileContent: content, fileName });
    } else {
      console.log('File save failed');
    }

  });

  socket.on('updating', ({ roomId, fileName }) => {
    // to all clients in the current namespace except the sender
    socket.broadcast.emit('updating', { roomId, fileName });
  });

  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });
});

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

const fetchFileList = () => {
  const files = fs.readdirSync('files').map((filename) => {
    const content = JSON.parse(fs.readFileSync(path.join('files', filename), 'utf-8'));
    return { name: filename, content };
  });

  return files;
};