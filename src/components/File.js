import React from 'react';
import './File.css';

export const File = ({ file, onSelect, onDownload }) => {

  return (
    <div className='fileWrap' key={file.name} >
      <div className='fileName' onClick={() => onSelect(file)}>{file.name}</div>
      <button onClick={() => onDownload(file.name)}>
        Download File
      </button>
    </div>
  );
};

export default File;
