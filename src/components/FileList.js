import React from 'react';
import File from './File';
import { SERVER_URL } from '../config';
import axios from 'axios';


export const FileList = ({ files, onFileSelect }) => {

  const handleFileDownload = async (name) => {

    try {
      // Fetch the content of the selected file from the server
      const response = await axios.get(`${SERVER_URL}/files/${name}`);
      
      // Create a Blob from the JSON content
      const blob = new Blob([JSON.stringify(response.data)], { type: 'application/json' });
      
      // Create a temporary link element to trigger the download
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = name;
      
      // Append the link to the document and trigger the click event
      document.body.appendChild(link);
      link.click();
      
      // Remove the link from the document
      document.body.removeChild(link);
    } catch (error) {
      console.error('Error downloading file:', error.message);
    }
  };

  return (
    <div>
      {files.map((file) => (
        <File key={file.name} file={file} onSelect={onFileSelect} onDownload={handleFileDownload} />
      ))}
    </div>
  );
};
