import React, { useState, useEffect } from 'react';
import axios from 'axios';
import io from 'socket.io-client';
import Modal from 'react-modal';
import { v4 as uuidv4 } from 'uuid';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/themes/prism.css';
import { SERVER_URL } from './config';
import './App.css';
import { FileList } from './components/FileList';

Modal.setAppElement('#root');

const socket = io(SERVER_URL);

function App() {
  const [fileData, setFileData] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);
  const [editContent, setEditContent] = useState('');
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [updatingUserData, setUpdatingUserData] = useState(null);
  const roomId = uuidv4();
  const isUserEditing = updatingUserData && 
  updatingUserData.roomId !== roomId && 
  selectedFile?.name === updatingUserData.fileName;

  useEffect(() => {
    const fetchInitialFileList = async () => {
      try {
        const response = await axios.get(`${SERVER_URL}/files`);
        setFileData(response.data);
      } catch (error) {
        console.error('Error fetching initial file list:', error.message);
      }
    };

    fetchInitialFileList();

    return () => {
      socket.disconnect();
    };
  }, []);

  useEffect(() => {
    socket.emit('join', roomId);

    socket.on('updating', ({ roomId, fileName }) => {
      setUpdatingUserData({ roomId, fileName });
    });

    return () => socket.off('updating');
  }, [roomId]);

  useEffect(() => {
    socket.on('updated', ({ fileList, newFileContent, fileName }) => {
      setFileData(fileList);

      if (fileName && fileName === selectedFile?.name) {
       setEditContent(JSON.stringify(newFileContent, null, 2));
      }
    });

    return () => socket.off('updated');

  }, [selectedFile]);

  const handleFileSelect = (file) => {
    setSelectedFile(file);
    setEditContent(JSON.stringify(file.content, null, 2));
    openEditModal();
  };

  const openEditModal = () => {
    setIsEditModalOpen(true);
  };

  const closeEditModal = () => {
    setIsEditModalOpen(false);
    setSelectedFile(null);
    socket.emit('updating', { roomId: null, fileName: null });
  };

  const handleEditSave = () => {
    socket.emit('update', { 
      fileName: selectedFile.name, 
      content: JSON.parse(editContent) 
    });
    socket.emit('updating', { roomId: null, fileName: null });
    setUpdatingUserData(null);
  };

  const onFileEdit = (code) => {
    socket.emit('updating', { roomId, fileName: selectedFile.name });
    setEditContent(code);
  }

  return (
    <div className='container'>
      <h1>File List</h1>

      <FileList onFileSelect={handleFileSelect} files={fileData} />

      <Modal
        isOpen={isEditModalOpen}
        onRequestClose={closeEditModal}
        className="edit-modal"
        overlayClassName="edit-overlay"
      >
        { isUserEditing && (
          <div className='updating'>User { updatingUserData.roomId } is updating the file</div>
        ) }

        <Editor
          value={editContent}
          onValueChange={onFileEdit}
          highlight={code => highlight(code, languages.js)}
          padding={10}
          className="editor"
        />

        <button onClick={handleEditSave}>Save</button>
        <button onClick={closeEditModal}>Close</button>
      </Modal>
    </div>
  );
}

export default App;